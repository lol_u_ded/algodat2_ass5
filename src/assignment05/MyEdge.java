package assignment05;

public class MyEdge {
    public int in, out; 	// incoming and outgoing indices of the vertices the an edge connects
    public int weight;		// weight of the edge
    
    MyEdge(){}
    
    MyEdge(int in, int out, int weight){
    	this.in = in;
    	this.out= out;
    	this.weight = weight;
    }
}
