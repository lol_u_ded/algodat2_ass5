package assignment05;


import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInfo;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestAssignment05_ext {

	private Roadmap map = null;
	private int linz = 0, stpoelten = 0, wien = 0, innsbruck = 0, bregenz = 0, eisenstadt = 0, graz = 0, klagenfurt = 0, salzburg = 0, passau = 0;
	
	private void initRoadmap() {
		map = new Roadmap();
		
		linz = map.insertVertex(new Station("Linz"));
		stpoelten = map.insertVertex(new Station("St.Poelten"));
		wien = map.insertVertex(new Station("Wien"));
		innsbruck = map.insertVertex(new Station("Innsbruck"));
		bregenz = map.insertVertex(new Station("Bregenz"));
		eisenstadt = map.insertVertex(new Station("Eisenstadt"));
		graz = map.insertVertex(new Station("Graz"));
		klagenfurt = map.insertVertex(new Station("Klagenfurt"));
		salzburg = map.insertVertex(new Station("Salzburg"));
		passau = map.insertVertex(new Station("Passau"));
		
		map.insertEdge(linz, stpoelten, 140);
		map.insertEdge(stpoelten, wien, 80);
		map.insertEdge(linz, wien, 200);
		map.insertEdge(wien, eisenstadt, 100);
		map.insertEdge(wien, graz, 190);
		map.insertEdge(graz, klagenfurt, 160);
		map.insertEdge(klagenfurt, salzburg, 210);
		map.insertEdge(linz, salzburg, 150);
		map.insertEdge(salzburg, innsbruck, 250);
		map.insertEdge(klagenfurt, innsbruck, 300);
		map.insertEdge(bregenz, innsbruck, 200);
	}
	
	@Test
	public void TestWienBregenz() {
		initRoadmap();
		System.out.println("Solution: ");
		System.out.println("Shortest distance from Wien to Bregenz");
		System.out.println("    over Linz: 200");
		System.out.println("    over Salzburg: 350");
		System.out.println("    over Innsbruck: 600");
		System.out.println("    to Bregenz: 800");
		System.out.println();
		System.out.println("Your output: ");
		ArrayList<Result> res = map.printShortestDistance(wien,bregenz);
		
		assertEquals(0, res.get(0).coveredDistance);
		assertEquals("Wien", res.get(0).station.name);
		assertEquals(200, res.get(1).coveredDistance);
		assertEquals("Linz", res.get(1).station.name);
		assertEquals(350, res.get(2).coveredDistance);
		assertEquals("Salzburg", res.get(2).station.name);
		assertEquals(600, res.get(3).coveredDistance);
		assertEquals("Innsbruck", res.get(3).station.name);
		assertEquals(800, res.get(res.size()-1).coveredDistance);
		assertEquals("Bregenz", res.get(res.size()-1).station.name);
		
		System.out.println("----------------------------------------------------------\n");
	}
	
	@Test
	public void TestInnsbruckEisenstadt() {
		initRoadmap();
		System.out.println("Solution: ");
		System.out.println("Shortest distance from Innsbruck to Eisenstadt");
		System.out.println("    over Salzburg: 250");
		System.out.println("    over Linz: 400");
		System.out.println("    over Wien: 600");
		System.out.println("    to Eisenstadt: 700");
		System.out.println();
		System.out.println("Your output: ");
		ArrayList<Result> res = map.printShortestDistance(innsbruck,eisenstadt);
		
		assertEquals(0, res.get(0).coveredDistance);
		assertEquals("Innsbruck", res.get(0).station.name);
		assertEquals(250, res.get(1).coveredDistance);
		assertEquals("Salzburg", res.get(1).station.name);
		assertEquals(400, res.get(2).coveredDistance);
		assertEquals("Linz", res.get(2).station.name);
		assertEquals(600, res.get(3).coveredDistance);
		assertEquals("Wien", res.get(3).station.name);
		assertEquals(700, res.get(res.size()-1).coveredDistance);
		assertEquals("Eisenstadt", res.get(res.size()-1).station.name);
		
		System.out.println("----------------------------------------------------------\n");
	}
	
	@Test
	public void TestWienKlagenfurt() {
		initRoadmap();
		System.out.println("Solution: ");
		System.out.println("Shortest distance from Wien to Klagenfurt");
		System.out.println("    over Graz: 190");
		System.out.println("    to Klagenfurt: 350");
		System.out.println();
		System.out.println("Your output: ");
		ArrayList<Result> res = map.printShortestDistance(wien,klagenfurt);
		
		assertEquals(0, res.get(0).coveredDistance);
		assertEquals("Wien", res.get(0).station.name);
		assertEquals(190, res.get(1).coveredDistance);
		assertEquals("Graz", res.get(1).station.name);
		assertEquals(350, res.get(res.size()-1).coveredDistance);
		assertEquals("Klagenfurt", res.get(res.size()-1).station.name);
		System.out.println("----------------------------------------------------------\n");
	}

	@Test
	public void TestEisenstadtKlagenfurt() {
		initRoadmap();
		ArrayList<Result> res = map.printShortestDistance(eisenstadt,klagenfurt);
		checkPath(new String[]{"Eisenstadt", "Wien", "Graz", "Klagenfurt"}, new int[] {0, 100, 290, 450}, res);
	}

	@Test
	public void TestAllDistances() {
		initRoadmap();
		int [] distances = map.printShortestDistances(wien);
		
		assertEquals(200, distances[linz]);
		assertEquals(80, distances[stpoelten]);
		assertEquals(0, distances[wien]);
		assertEquals(600, distances[innsbruck]);
		assertEquals(800, distances[bregenz]);
		assertEquals(100, distances[eisenstadt]);
		assertEquals(190, distances[graz]);
		assertEquals(350, distances[klagenfurt]);
		assertEquals(350, distances[salzburg]);
		assertEquals(-1, distances[passau]);
		System.out.println("----------------------------------------------------------\n");
	}

	private Roadmap myMap;
	private int n0, n1, n2, n3, n4, n5, n6, n7, n8 = 0;

	@Test
	public void bigTest() {
		myMap = new Roadmap();

		n0 = myMap.insertVertex(new Station("0"));
		n1 = myMap.insertVertex(new Station("1"));
		n2 = myMap.insertVertex(new Station("2"));
		n3 = myMap.insertVertex(new Station("3"));
		n4 = myMap.insertVertex(new Station("4"));
		n5 = myMap.insertVertex(new Station("5"));
		n6 = myMap.insertVertex(new Station("6"));
		n7 = myMap.insertVertex(new Station("7"));
		n8 = myMap.insertVertex(new Station("8"));

		myMap.insertEdge(n0, n1, 4);
		myMap.insertEdge(n1, n2, 8);
		myMap.insertEdge(n2, n3, 7);
		myMap.insertEdge(n3, n4, 9);
		myMap.insertEdge(n4, n5, 10);
		myMap.insertEdge(n5, n3, 14);
		myMap.insertEdge(n5, n2, 4);
		myMap.insertEdge(n5, n6, 2);
		myMap.insertEdge(n6, n8, 6);
		myMap.insertEdge(n8, n2, 2);
		myMap.insertEdge(n6, n7, 1);
		myMap.insertEdge(n8, n7, 7);
		myMap.insertEdge(n7, n0, 8);
		myMap.insertEdge(n7, n1, 11);

		int[] costs = myMap.printShortestDistances(n0);
		assertEquals(0, costs[n0]);
		assertEquals(4, costs[n1]);
		assertEquals(12, costs[n2]);
		assertEquals(19, costs[n3]);
		assertEquals(21, costs[n4]);
		assertEquals(11, costs[n5]);
		assertEquals(9, costs[n6]);
		assertEquals(8, costs[n7]);
		assertEquals(14, costs[n8]);

		ArrayList<Result> results = myMap.printShortestDistance(n0, n1);
		checkPath(new String[]{"0", "1"}, new int[] {0, 4}, results);

		results = myMap.printShortestDistance(n0, n2);
		checkPath(new String[]{"0", "1", "2"}, new int[] {0, 4, 12}, results);

		results = myMap.printShortestDistance(n0, n3);
		checkPath(new String[]{"0", "1", "2", "3"}, new int[] {0, 4, 12, 19}, results);

		results = myMap.printShortestDistance(n0, n4);
		checkPath(new String[]{"0", "7", "6", "5", "4"}, new int[] {0, 8, 9, 11, 21}, results);

		results = myMap.printShortestDistance(n0, n5);
		checkPath(new String[]{"0", "7", "6", "5"}, new int[] {0, 8, 9, 11}, results);

		results = myMap.printShortestDistance(n0, n6);
		checkPath(new String[]{"0", "7", "6"}, new int[] {0, 8, 9}, results);

		results = myMap.printShortestDistance(n0, n7);
		checkPath(new String[]{"0", "7"}, new int[] {0, 8}, results);

		results = myMap.printShortestDistance(n0, n8);
		checkPath(new String[]{"0", "1", "2", "8"}, new int[] {0, 4, 12, 14}, results);

		results = myMap.printShortestDistance(n8, n0);
		checkPath(new String[]{"8", "2", "1", "0"}, new int[] {0, 2, 10, 14}, results);
	}

	private void checkPath(String[] names, int[] distances, ArrayList<Result> results) {
		for (int i = 0; i < results.size(); i++) {
			assertEquals(names[i], results.get(i).station.name);
			assertEquals(distances[i], results.get(i).coveredDistance);
		}
	}

	// ...
	// ...
}
