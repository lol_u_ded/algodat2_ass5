package assignment05;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

public class TestAssignment05_adjMatrix {

	// adjMatrix for test
	// *** since we're dealing with undirected graphs, only one direction is given in the adj-matrix, even though
	// *** that's mathematically incorrect :)
	final private int[][][] testConfigs = {
			{
					{0, 1, 100, 101, 102, 103, 104},
					{-1, 0, 1, -1, -1, -1, -1},
					{-1, -1, 0, 1, -1, -1, -1},
					{-1, -1, -1, 0, 1, -1, -1},
					{-1, -1, -1, -1, 0, 1, -1},
					{-1, -1, -1, -1, -1, 0, 1},
					{-1, -1, -1, -1, -1, -1, 0}
			},
			{
					{0, -1, -1, -1, -1, -1, 1},
					{-1, 0, -1, -1, -1, -1, -1},
					{-1, -1, 0, -1, -1, -1, -1},
					{-1, -1, -1, 0, -1, -1, -1},
					{-1, -1, -1, -1, 0, -1, -1},
					{-1, -1, -1, -1, -1, 0, -1},
					{-1, -1, -1, -1, -1, -1, 0}
			},
			{
					{0, 10, 100, -1, -1, -1, -1},
					{-1, 0, 1, 100, -1, -1, -1},
					{-1, -1, 0, 1, 100, -1, -1},
					{-1, -1, -1, 0, 1, 100, -1},
					{-1, -1, -1, -1, 0, 1, 100},
					{-1, -1, -1, -1, -1, 0, 1},
					{-1, -1, -1, -1, -1, -1, 0}
			},
			{
					{0, -1, -1, -1, -1, 1, -1},
					{-1, 0, -1, -1, 1, -1, -1},
					{-1, -1, 0, 1, -1, -1, -1},
					{-1, -1, -1, 0, -1, -1, 1},
					{-1, -1, 1, -1, 0, -1, -1},
					{-1, 1, -1, -1, -1, 0, -1},
					{-1, -1, -1, -1, -1, -1, 0}
			},
			{
					{0, 1, 1, -1, -1, -1, -1},
					{-1, 0, -1, 1, -1, -1, -1},
					{-1, -1, 0, -1, 1, -1, -1},
					{-1, -1, -1, 0, -1, 1, -1},
					{-1, -1, -1, -1, 0, -1, 1},
					{-1, -1, -1, -1, -1, 0, 1},
					{-1, -1, -1, -1, -1, -1, 0}
			},
			{
					{0, 10, -1, -1, -1, -1, 100},
					{-1, 0, 10, -1, -1, -1, 100},
					{-1, -1, 0, 10, -1, -1, 100},
					{-1, -1, -1, 0, 10, -1, 100},
					{-1, -1, -1, -1, 0, 10, 100},
					{-1, -1, -1, -1, -1, 0, 10},
					{-1, -1, -1, -1, -1, -1, 0}
			}
	};
	// arrays from printShortestDistances
	final private int[][] testDists = {
			{0, 1, 2, 3, 4, 5, 6},
			{0, -1, -1, -1, -1, -1, 1},
			{0, 10, 11, 12, 13, 14, 15},
			{0, 2, 4, 5, 3, 1, 6},
			{0, 1, 1, 2, 2, 3, 3},
			{0, 10, 20, 30, 40, 50, 60}
	};
	// dists in ArraysList<Result>
	// each array has to be as long as it's corresponding result.size()
	final private int[][] testResultsDists = {
			{0, 1, 2, 3, 4, 5, 6},
			{0, 1},
			{0, 10, 11, 12, 13, 14, 15},
			{0, 1, 2, 3, 4, 5, 6},
			{0, 1, 2, 3},
			{0, 10, 20, 30, 40, 50, 60}
	};
	// Names in ArraysList<Result>
	// each array has to be as long as it's corresponding result.size()
	final private String[][] testResultsPath = {
			{"A", "B", "C", "D", "E", "F", "G"},
			{"A", "G"},
			{"A", "B", "C", "D", "E", "F", "G"},
			{"A", "F", "B", "E", "C", "D", "G"},
			{"A", "C", "E", "G"},
			{"A", "B", "C", "D", "E", "F", "G"}
	};
	private Roadmap map = null;
	private int A = 0, B = 0, C = 0, D = 0, E = 0, F = 0, G = 0;

	private void initWithTestConfig(int i) {
		map = new Roadmap();

		A = map.insertVertex(new Station("A"));
		B = map.insertVertex(new Station("B"));
		C = map.insertVertex(new Station("C"));
		D = map.insertVertex(new Station("D"));
		E = map.insertVertex(new Station("E"));
		F = map.insertVertex(new Station("F"));
		G = map.insertVertex(new Station("G"));

		int[][] matrix = testConfigs[i];
		for (int j = 0; j < matrix.length; j++) {
			for (int l = 0; l < matrix[j].length; l++) {
				if (matrix[j][l] > 0) {
					try {
						map.insertEdge(j, l, matrix[j][l]);
					} catch (IllegalArgumentException ignored) {
					}
				}
			}
		}
	}

	@Test
	@DisplayName("Test: Config 0")
	public void TestConfig0(TestInfo testInfo) {
		testConfig(0);
	}

	@Test
	@DisplayName("Test: Config 1")
	public void TestConfig1(TestInfo testInfo) {
		testConfig(1);
	}

	@Test
	@DisplayName("Test: Config 2")
	public void TestConfig2(TestInfo testInfo) {
		testConfig(2);
	}

	@Test
	@DisplayName("Test: Config 3")
	public void TestConfig3(TestInfo testInfo) {
		testConfig(3);
	}

	@Test
	@DisplayName("Test: Config 4")
	public void TestConfig4(TestInfo testInfo) {
		testConfig(4);
	}

	@Test
	@DisplayName("Test: Config 5")
	public void TestConfig5(TestInfo testInfo) {
		testConfig(5);
	}

	private void testConfig(int index) {
		initWithTestConfig(index);
		// test printShortestDistances from A
		int[] test = map.printShortestDistances(A);
		assertArrayEquals(testDists[index], test);

		// test printShortestDistance from A to G
		ArrayList<Result> results = map.printShortestDistance(A, G);
		resultEquals(index, results);
	}

	private void resultEquals(int i, ArrayList<Result> result) {
		int[] dists = result.stream().mapToInt(r -> r.coveredDistance).toArray();
		String[] path = result.stream().map(r -> r.station.name).toArray(String[]::new);

		assertArrayEquals(testResultsDists[i], dists);
		assertArrayEquals(testResultsPath[i], path);
	}
}
