package assignment05;

public class Result {
	protected MyVertex station;		// station
	protected int coveredDistance;  // covered distance from the start to that station
	
	Result(MyVertex station, int distance){
		this.station = station;
		this.coveredDistance = distance;
	}
}
