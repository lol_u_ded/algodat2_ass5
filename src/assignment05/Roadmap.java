package assignment05;

import java.util.ArrayList;
import java.util.Arrays;

public class Roadmap extends Graph {



	/**
	 * This method determines and prints the shortest path between two stations (= vertex indices) "from" and "to",
	 * using the dijkstra algorithm. 
	 * @param from
	 * 		vertex index of the start station
	 * @param to
	 * 		vertex index of the destination station
	 * @return
	 *      The path with the already covered distance is returned as ArrayList. This list contains sequentially 
	 *      each station along the shortest path, together with the already covered distance.
	 *      (see example on the assignment sheet)
	 */
	public ArrayList<Result> printShortestDistance(int from, int to) {

		DikstraObj d = dijkstra(from, to);

		ArrayList<Result> results = getCorrectPath(d.minDist, d.adj, d.results, from, to);

		System.out.println("Shortest distance from " + vertices[from].name + " to " + vertices[to].name);
		for(int r = 1; r < results.size(); r++){
			if(r == results.size()-1) {
				System.out.println("    to " + results.get(r).station + ": " + results.get(r).coveredDistance);
			}else{
				System.out.println("    over " + results.get(r).station + ": " + results.get(r).coveredDistance);
			}
		}
		return results;
	}


	/**
	 * This method determines and prints the shortest path from station (= vertex index) "from" to all other stations
	 * using the dijkstra algorithm, and returns them in an array.
	 *  
	 * @param from
	 * 		vertex index of the start station
	 * @return
	 * 		Returns the results in form of an int[] array, where the indices correspond to the indices of the vertices.
	 *      (see example on the assignment sheet)
	 */
	public int[] printShortestDistances(int from) {
		DikstraObj dijkstra = dijkstra(from);
		System.out.println("from " + vertices[from].name );
		for(int idx = 0; idx < dijkstra.minDist.length; idx++){
			System.out.println("   to " + vertices[idx].name +": " + dijkstra.minDist[idx]);
		}
		return dijkstra.minDist;
	}

	// Auxiliary Methods & additional variables

	/* used for the method printShortestDistances(int from) */
	private DikstraObj dijkstra(int from){

		/* reset Array & fill it with default values
		 * additionally reset array that shows which nodes are already visited.
		 */

		DikstraObj d = new DikstraObj(from,-1);
		int curr = from;

		/* cycle through every node as long there is something to cycle */
		while(curr != -1){
			/* check and update distances if necessary */
			for(int col = 0; col < d.length; col++ ){
				if(d.adj[curr][col] == 1 ){
					int newDistance;
					/* if there is no value the distance has to be updated anyways*/
					if(d.minDist[col] == -1){
						d.minDist[col] = hasEdge(curr,col) + d.minDist[curr];
					}else /* in any other case a check is needed that tests if the path is shorter than before*/{
						newDistance = d.minDist[curr] + hasEdge(curr, col);
						if(newDistance < d.minDist[col] ){
							d.minDist[col] = newDistance;
						}
					}
				}
			}
			d.visited[curr] = true;
			curr = findLocalMin(d);
		}
		return d;
	}

	/* Used for the method printShortestDistance(int from, int to)*/
	private DikstraObj dijkstra(int from, int to){

		/* reset Array & fill it with default values
		 * additionally reset array that shows which nodes are already visited.
		 */
		DikstraObj d = new DikstraObj(from,-1);

		String prev = "None";
		int curr = from;
		d.results.add(new Result(vertices[from],0));

		/* cycle through every node as long there is something to cycle */
		while(curr != -1){
			/* check and update distances if necessary */
			for (int col = 0; col < d.length; col++) {
				if (d.adj[curr][col] == 1) {
					int newDistance;

					/* this is a completely useless value */
					d.neighbors.get(col).add(prev);

					/* if there is no value the distance has to be updated anyways*/
					if (d.minDist[col] == -1) {
						d.minDist[col] = hasEdge(curr, col) + d.minDist[curr];
						d.results.add(new Result(vertices[col], hasEdge(curr, col) + d.minDist[curr]));

					} else /* in any other case a check is needed that tests if the path is shorter than before*/ {
						newDistance = d.minDist[curr] + hasEdge(curr, col);
						if (newDistance < d.minDist[col]) {
							d.minDist[col] = newDistance;

							/* search & delete to prevent double values */
							update(vertices[col].name, d);
							d.results.add(new Result(vertices[col], newDistance));
						}
					}
				}
			}

			d.visited[curr] = true;
			prev = vertices[curr].name;
			if (curr == to) {
				curr = -1;
			}else{
				curr = findLocalMin(d);
			}
		}
		return d;
	}

	/* This method will return the index of next LocalMinimum from minDist array */
	private int findLocalMin(DikstraObj d) {
		int index = -1;
		int min = Integer.MAX_VALUE;
		for(int i=0; i < d.minDist.length;i++){
			if(d.minDist[i] > 0 && !d.visited[i]){
				if(d.minDist[i] < min){
					min = d.minDist[i];
					index = i;
				}
			}
		}
		return index;
	}

	/* An objekt that store some variables */
	private class DikstraObj {

		int [] minDist;
		boolean[] visited;
		ArrayList<ArrayList<String>> neighbors;
		ArrayList<Result> results;
		int[][] adj;
		int length;
		int from;
		int to;

		DikstraObj(int from, int to){
			this.from = from;
			this.to = to;
			length = getNumberOfVertices();
			minDist  = new int[length];
			visited = new boolean[length];
			Arrays.fill(minDist, -1);
			Arrays.fill(visited, false);
			minDist[from] = 0;
			adj = getAdjacencyMatrix();
			results = new ArrayList<>();
			neighbors = new ArrayList<>();
			for(int i=0; i < length; i++){
				neighbors.add(new ArrayList<>());
			}
		}

	}

	/* This method will delete duplicates out of the result Array List */
	private void update(String vertex, DikstraObj d) {
		for(int i = 0; i < d.results.size(); i++){
			if(d.results.get(i).station.name.equals(vertex)){
				d.results.remove(i);
				break;
			}
		}
	}

	/* This method should now normalise the results to the right order and deletes stupid sh*t */
	private ArrayList<Result> getCorrectPath(int[] minDist, int[][] adj, ArrayList<Result> results, int from, int to) {
		boolean[] v = new boolean[getNumberOfVertices()];
		int minDistance = minDist[to];
		Arrays.fill(v,false);

		ArrayList<Result> updatedResult = new ArrayList<>();

		/* exclude all redundant nodes */
		for(int i = 0; i < minDist.length; i++){
			if (minDist[i] == -1 || minDist[i] > minDist[to]) {
				v[i] = true;
			}
			/* If nod excluded in step one test the min distance */
			if (!v[i] && i != to) {
				int[] currTest = dijkstra(i).minDist;
				v[i] = (currTest[to] + minDist[i]) > minDistance;
			}
		}

		/* get the updated Result and return it */
		for (Result r : results) {
			int idx = findIndex(minDist, r.coveredDistance, v, r);
			if (idx >= 0) {
				if (!v[idx]) {
					updatedResult.add(r);
				}
			}
		}
		return updatedResult;
	}

	/* this Method will find the correct nodes to be added */
	private int findIndex(int[] arr, int target, boolean[] b, Result r) {
		if (arr == null) {
			return -1;
		}
		int i = 0;
		while (i < arr.length) {
			if (arr[i] == target && !b[i] && r.station.equals(vertices[i])) return i;
			else i++;
		}
		return -1;
	}

}
