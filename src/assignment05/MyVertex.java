package assignment05;

public class MyVertex {
	protected String name;
    public MyVertex(String Name) {
        this.name = Name;
    }
    
    // returns a vertex in form of a string.
    public String toString() {
        return this.name;
    }
}
