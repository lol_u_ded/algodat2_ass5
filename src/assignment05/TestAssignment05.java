package assignment05;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

public class TestAssignment05 {

	private Roadmap map = null;
	private int linz = 0, stpoelten = 0, wien = 0, innsbruck = 0, bregenz = 0, eisenstadt = 0, graz = 0, klagenfurt = 0, salzburg = 0, passau = 0;
	
	private void initRoadmap() {
		map = new Roadmap();
		
		linz = map.insertVertex(new Station("Linz"));
		stpoelten = map.insertVertex(new Station("St.Poelten"));
		wien = map.insertVertex(new Station("Wien"));
		innsbruck = map.insertVertex(new Station("Innsbruck"));
		bregenz = map.insertVertex(new Station("Bregenz"));
		eisenstadt = map.insertVertex(new Station("Eisenstadt"));
		graz = map.insertVertex(new Station("Graz"));
		klagenfurt = map.insertVertex(new Station("Klagenfurt"));
		salzburg = map.insertVertex(new Station("Salzburg"));
		passau = map.insertVertex(new Station("Passau"));
		
		map.insertEdge(linz, stpoelten, 140);
		map.insertEdge(stpoelten, wien, 80);
		map.insertEdge(linz, wien, 200);
		map.insertEdge(wien, eisenstadt, 100);
		map.insertEdge(wien, graz, 190);
		map.insertEdge(graz, klagenfurt, 160);
		map.insertEdge(klagenfurt, salzburg, 210);
		map.insertEdge(linz, salzburg, 150);
		map.insertEdge(salzburg, innsbruck, 250);
		map.insertEdge(klagenfurt, innsbruck, 300);
		map.insertEdge(bregenz, innsbruck, 200);
	}
	
	@Test
	@DisplayName("Test: from Wien to Bregenz")
	public void TestWienBregenz(TestInfo testInfo) {
		initRoadmap();
		System.out.println("\n-------"+testInfo.getDisplayName()+"-------");
		System.out.println("Solution: ");
		System.out.println("Shortest distance from Wien to Bregenz");
		System.out.println("    over Linz: 200");
		System.out.println("    over Salzburg: 350");
		System.out.println("    over Innsbruck: 600");
		System.out.println("    to Bregenz: 800");
		System.out.println();
		System.out.println("Your output: ");
		ArrayList<Result> res = map.printShortestDistance(wien,bregenz);
		
		assertEquals(0, res.get(0).coveredDistance);
		assertEquals("Wien", res.get(0).station.name);
		assertEquals(200, res.get(1).coveredDistance);
		assertEquals("Linz", res.get(1).station.name);
		assertEquals(350, res.get(2).coveredDistance);
		assertEquals("Salzburg", res.get(2).station.name);
		assertEquals(600, res.get(3).coveredDistance);
		assertEquals("Innsbruck", res.get(3).station.name);
		assertEquals(800, res.get(res.size()-1).coveredDistance);
		assertEquals("Bregenz", res.get(res.size()-1).station.name);
		
		System.out.println("----------------------------------------------------------\n");
	}
	
	@Test
	@DisplayName("Test: from Innsbruck to Eisenstadt")
	public void TestInnsbruckEisenstadt(TestInfo testInfo) {
		
		initRoadmap();
		System.out.println("-------"+testInfo.getDisplayName()+"-------");
		System.out.println("Solution: ");
		System.out.println("Shortest distance from Innsbruck to Eisenstadt");
		System.out.println("    over Salzburg: 250");
		System.out.println("    over Linz: 400");
		System.out.println("    over Wien: 600");
		System.out.println("    to Eisenstadt: 700");
		System.out.println();
		System.out.println("Your output: ");
		ArrayList<Result> res = map.printShortestDistance(innsbruck,eisenstadt);
		
		assertEquals(0, res.get(0).coveredDistance);
		assertEquals("Innsbruck", res.get(0).station.name);
		assertEquals(250, res.get(1).coveredDistance);
		assertEquals("Salzburg", res.get(1).station.name);
		assertEquals(400, res.get(2).coveredDistance);
		assertEquals("Linz", res.get(2).station.name);
		assertEquals(600, res.get(3).coveredDistance);
		assertEquals("Wien", res.get(3).station.name);
		assertEquals(700, res.get(res.size()-1).coveredDistance);
		assertEquals("Eisenstadt", res.get(res.size()-1).station.name);
		
		System.out.println("----------------------------------------------------------\n");
	}
	
	@Test
	@DisplayName("Test: from Wien to Klagenfurt")
	public void TestWienKlagenfurt(TestInfo testInfo) {
	
		initRoadmap();
		System.out.println("-------"+testInfo.getDisplayName()+"-------");
		System.out.println("Solution: ");
		System.out.println("Shortest distance from Wien to Klagenfurt");
		System.out.println("    over Graz: 190");
		System.out.println("    to Klagenfurt: 350");
		System.out.println();
		System.out.println("Your output: ");
		ArrayList<Result> res = map.printShortestDistance(wien,klagenfurt);
		
		assertEquals(0, res.get(0).coveredDistance);
		assertEquals("Wien", res.get(0).station.name);
		assertEquals(190, res.get(1).coveredDistance);
		assertEquals("Graz", res.get(1).station.name);
		assertEquals(350, res.get(res.size()-1).coveredDistance);
		assertEquals("Klagenfurt", res.get(res.size()-1).station.name);
		System.out.println("----------------------------------------------------------\n");
	}
	
	@Test
	@DisplayName("Test: Distances from Wien to all stations")
	public void TestAllDistances(TestInfo testInfo) {
		initRoadmap();
		System.out.println("-------"+testInfo.getDisplayName()+"-------");
		int [] distances = map.printShortestDistances(wien);
		
		assertEquals(200, distances[linz]);
		assertEquals(80, distances[stpoelten]);
		assertEquals(0, distances[wien]);
		assertEquals(600, distances[innsbruck]);
		assertEquals(800, distances[bregenz]);
		assertEquals(100, distances[eisenstadt]);
		assertEquals(190, distances[graz]);
		assertEquals(350, distances[klagenfurt]);
		assertEquals(350, distances[salzburg]);
		assertEquals(-1, distances[passau]);
		System.out.println("----------------------------------------------------------\n");
	}
	
	// ...
	// ...
}
