package assignment05;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;


public class TestAssignment4Graph {
	
	private final boolean undirectedGraphFlag = true;
	
	private Graph g;  
	
	
	@Test
	public void testInsertVertex() {
		try {
			Graph map = new Graph();
			
			MyVertex vertexLinz = new MyVertex("Linz");
			int idxLinz = map.insertVertex(vertexLinz);
			
			assertEquals(".insertVertex() returned wrong vertex at index 0 after inserting vertex \"Linz\" into an empty graph: ", vertexLinz, map.vertices[idxLinz]);
			assertEquals(1, map.numVertices);
			
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	@Test
	public void testInsertMultipleVertices() {
		try {
			Graph map = new Graph();
			
			MyVertex vertex1Linz = new MyVertex("Linz");
			MyVertex vertex2Wien = new MyVertex("Wien");
			MyVertex vertex3Graz = new MyVertex("Graz");
			
			int idxLinz = map.insertVertex(vertex1Linz);
			assertEquals(".insertVertex() returned wrong index after inserting vertex \"Linz\" into an empty graph: ", 0, idxLinz);
			assertEquals(".insertVertex(): wrong vertex at index 0 after inserting vertex \"Linz\" into an empty graph: ", vertex1Linz, map.vertices[idxLinz]);
			
			int idxWien = map.insertVertex(vertex2Wien);
			assertEquals(".insertVertex() returned wrong index for \"Wien\" after inserting vertex \"Linz\" and \"Wien\" into an empty graph: ", 1, idxWien);
			assertEquals(".insertVertex(): wrong vertex at index 0 after inserting vertices \"Linz\" and \"Wien\" into an empty graph: ", vertex1Linz, map.vertices[idxLinz]);
			assertEquals(".insertVertex(): wrong vertex at index 1 after inserting vertices \"Linz\" and \"Wien\" into an empty graph: ", vertex2Wien, map.vertices[idxWien]);
			
			int idxGraz = map.insertVertex(vertex3Graz);
			assertEquals(".insertVertex() returned wrong index for \"Graz\" after inserting vertex \"Linz\", \"Wien\" and \"Graz\" into an empty graph: ", 2, idxGraz);
			assertEquals(".insertVertex(): wrong vertex at index 0 after inserting vertices \"Linz\", \"Wien\" and \"Graz\" into an empty graph: ", vertex1Linz, map.vertices[idxLinz]);
			assertEquals(".insertVertex(): wrong vertex at index 1 after inserting vertices \"Linz\", \"Wien\" and \"Graz\" into an empty graph: ", vertex2Wien, map.vertices[idxWien]);
			assertEquals(".insertVertex(): wrong vertex at index 2 after inserting vertices \"Linz\", \"Wien\" and \"Graz\" into an empty graph: ", vertex3Graz, map.vertices[idxGraz]);
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	
	@Test
	public void testGetNumberOfVerticesSimple() {
		try {
			Graph map = new Graph();
			
			MyVertex vertex1Linz = new MyVertex("Linz");
			MyVertex vertex2Wien = new MyVertex("Wien");
			MyVertex vertex3Graz = new MyVertex("Graz");
			
			map.insertVertex(vertex1Linz);
			map.insertVertex(vertex2Wien);
			map.insertVertex(vertex3Graz);
			
			assertEquals(".getNumberOfVertices() returned wrong value for a graph with 3 vertices: ", 3, map.getNumberOfVertices());
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	
	@Test
	public void testGetNumberOfVerticesLarge() {
		try {
			g = this.initializeLargeGraph();
			assertEquals(".getNumberOfVertices() returned wrong value for a graph with 18 vertices: ", 18, g.getNumberOfVertices());
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	
	@Test
	public void testInsertNotExistingEdges() {
		try {
			Graph map = new Graph();
			
			int linz = map.insertVertex(new MyVertex("Linz"));
			int wien = map.insertVertex(new MyVertex("Wien"));
			int graz = map.insertVertex(new MyVertex("Graz"));
			String strGraph = "[\"Linz\",\"Wien\",\"Graz\"]";
			
			assertTrue(".insertEdge(): inserting a not existing edge between \"Linz\" and \"Wien\" in a graph with vertices "+strGraph+" returned false.", map.insertEdge(linz,  wien,  100));
			assertTrue(".insertEdge(): inserting a not existing edge between \"Graz\" and \"Wien\" in a graph with vertices "+strGraph+" returned false.", map.insertEdge(graz,  wien,  10));
			assertTrue(".insertEdge(): inserting a not existing edge between \"Linz\" and \"Graz\" in a graph with vertices "+strGraph+" returned false.", map.insertEdge(linz,  graz,  1));
			
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	@Test
	public void testInsertExistingEdge() {
		try {
			Graph map = new Graph();
			
			int linz = map.insertVertex(new MyVertex("Linz"));
			int wien = map.insertVertex(new MyVertex("Wien"));
			int graz = map.insertVertex(new MyVertex("Graz"));
			String strGraph = "[\"Linz\",\"Wien\",\"Graz\"]";
			
			assertTrue(".insertEdge(): inserting a not existing edge between \"Linz\" and \"Wien\" in a graph with vertices "+strGraph+" returned false.", map.insertEdge(linz,  wien,  100));
			assertTrue(".insertEdge(): inserting a not existing edge between \"Graz\" and \"Wien\" in a graph with vertices "+strGraph+" returned false.", map.insertEdge(graz,  wien,  10));
			assertTrue(".insertEdge(): inserting a not existing edge between \"Linz\" and \"Graz\" in a graph with vertices "+strGraph+" returned false.", map.insertEdge(linz,  graz,  1));
			
			assertFalse(".insertEdge(): inserting an already existing edge between \"Linz\" and \"Graz\" in a graph with vertices "+strGraph+" returned true.", map.insertEdge(linz,  graz,  1));			
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	@Test
	public void testGetEdges() {
		try {
			Graph map = new Graph();
			
			int linz = map.insertVertex(new MyVertex("Linz"));
			int wien = map.insertVertex(new MyVertex("Wien"));
			int graz = map.insertVertex(new MyVertex("Graz"));
			String strGraph = "[\"Linz\",\"Wien\",\"Graz\"]";
			
			map.insertEdge(linz, wien, 100);
			MyEdge[] test = map.getEdges();
			assertEquals(".getEdges() returns array with wrong size for the graph "+strGraph+" and 1 inserted edge.",1, test.length);
			
			// as we have an undirected graph, we also have to test for a possible edge in opposite direction 
			assertTrue(".getEdges(): Did not find correct edge at index 0 after inserting the first edge (\"Wien\",\"Linz\",100) in the graph "+strGraph+".",(wien == test[0].in && linz == test[0].out) || (wien == test[0].out && linz == test[0].in)); 
			assertEquals(".getEdges(): The first inserted edge (\"Wien\",\"Linz\",100) in the graph "+strGraph+" has wrong weight: ",100, test[0].weight);
			
			map.insertEdge(wien, graz, 50);
			test = map.getEdges();
			assertEquals(".getEdges() returns array with wrong size for the graph "+strGraph+" and 2 inserted edges.",2, test.length);
			
			// as we have an undirected graph, we also have to test for a possible edge in opposite direction 
			assertTrue(".getEdges(): Did not find correct edge at index 0 after inserting the first edge (\"Wien\",\"Linz\",100) in the graph "+strGraph+".",(wien == test[0].in && linz == test[0].out) || (wien == test[0].out && linz == test[0].in)); 
			assertEquals(".getEdges(): The first inserted edge (\"Wien\",\"Linz\",100) in the graph "+strGraph+" has wrong weight: ",100, test[0].weight);
			
			assertTrue(".getEdges(): Did not find correct edge at index 1 after inserting the 2nd edge (\"Wien\",\"Graz\",50) in the graph "+strGraph+".",(graz == test[1].in && wien == test[1].out) || (graz == test[1].out && wien == test[1].in));
			assertEquals(".getEdges(): The 2nd inserted edge (\"Wien\",\"Graz\",50) in the graph "+strGraph+" has wrong weight: ",50, test[1].weight);
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	@Test
	public void testCatchLoop() {
		Exception exCaught = null;
		try{
			Graph map = new Graph();
			int linz = map.insertVertex(new MyVertex("Linz"));
		
			map.insertEdge(linz, linz, (int)Math.random()*100);
		} catch (Exception e) {
			exCaught = e;
		}
		assertTrue(".insertEdge() did not throw an IllegalArgumentException in case of inserting a loop: "+exCaught, exCaught instanceof IllegalArgumentException);
	}
	

	@Test
	public void testGetVertices() {
		try {
			g = this.initializeGraph2Components();
			String strGraph = "[\"Linz\",\"St.Poelten\",\"Wien\",\"Innsbruck\",\"Bregenz\",\"Eisenstadt\",\"Graz\",\"Klagenfurt\",\"Salzburg\",\"London\"]";
			MyVertex[] v = g.getVertices();
			assertEquals(".getVertices() returned array of wrong size for the graph with vertices "+strGraph,10,v.length);
			
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Linz\" should be at index 0.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[0].toString().equals("Linz"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"St.Poelten\" should be at index 1.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[1].toString().equals("St.Poelten"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Wien\" should be at index 2.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[2].toString().equals("Wien"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Innsbruck\" should be at index 3.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[3].toString().equals("Innsbruck"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Bregenz\" should be at index 4.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[4].toString().equals("Bregenz"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Eisenstadt\" should be at index 5.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[5].toString().equals("Eisenstadt"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Graz\" should be at index 6.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[6].toString().equals("Graz"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Klagenfurt\" should be at index 7.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[7].toString().equals("Klagenfurt"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Salzburg\" should be at index 8.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[8].toString().equals("Salzburg"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"London\" should be at index 9.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[9].toString().equals("London"));
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}

	@Test
	public void testGetVerticesLarge() {
		try {
			g = this.initializeLargeGraph();
			String strGraph = "[\"Linz\",\"St.Poelten\",\"Wien\",\"Innsbruck\",\"Bregenz\",\"Eisenstadt\",\"Graz\",\"Klagenfurt\",\"Salzburg\",\"A\",\"B\",\"C\",\"D\",\"E\",\"F\",\"G\",\"H\",\"I\"]";
			
			MyVertex[] v = g.getVertices();
			assertEquals(".getVertices() returned array of wrong size for the graph with 18 vertices "+strGraph,18, v.length);
			
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Linz\" should be at index 0.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[0].toString().equals("Linz"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"St.Poelten\" should be at index 1.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[1].toString().equals("St.Poelten"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Wien\" should be at index 2.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[2].toString().equals("Wien"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Innsbruck\" should be at index 3.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[3].toString().equals("Innsbruck"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Bregenz\" should be at index 4.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[4].toString().equals("Bregenz"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Eisenstadt\" should be at index 5.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[5].toString().equals("Eisenstadt"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Graz\" should be at index 6.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[6].toString().equals("Graz"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Klagenfurt\" should be at index 7.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[7].toString().equals("Klagenfurt"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"Salzburg\" should be at index 8.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[8].toString().equals("Salzburg"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"A\" should be at index 9.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[9].toString().equals("A"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"B\" should be at index 10.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[10].toString().equals("B"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"C\" should be at index 11.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[11].toString().equals("C"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"D\" should be at index 12.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[12].toString().equals("D"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"E\" should be at index 13.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[13].toString().equals("E"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"F\" should be at index 14.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[14].toString().equals("F"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"G\" should be at index 15.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[15].toString().equals("G"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"H\" should be at index 16.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[16].toString().equals("H"));
			assertTrue(".getVertices(): For the graph with the vertices "+strGraph+" the vertex \"I\" should be at index 17.\n"
					+"\tYour vertices array is: "+printVerticesArray(g),v[17].toString().equals("I"));
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	@Test
	public void testHasEdgeForUndirectedGraph() {
		try {
			g = this.initializeLargeGraph();

			String strEdges = "(v1,v2,weight) [(0,2,1),(2,5,2),(2,6,3),(6,7,4),(4,3,5),(7,3,6),(8,3,7),(8,10,8),(7,11,9),(1,9,10),(10,9,11),(13,14,12),(14,15,13),(15,16,14),(16,12,15),(16,17,16)]";
			
			assertEquals(".hasEdge() returned a wrong weight for the edge (0,2) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,1, g.hasEdge(0, 2));
						
			assertEquals(".hasEdge() returned a wrong weight for the edge (2,5) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,2, g.hasEdge(2, 5));
			assertEquals(".hasEdge() returned a wrong weight for the edge (2,6) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,3, g.hasEdge(2, 6));
			assertEquals(".hasEdge() returned a wrong weight for the edge (6,7) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,4, g.hasEdge(6, 7));
			assertEquals(".hasEdge() returned a wrong weight for the edge (4,3) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,5, g.hasEdge(4, 3));
			assertEquals(".hasEdge() returned a wrong weight for the edge (8,3) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,7, g.hasEdge(8, 3));
			assertEquals(".hasEdge() returned a wrong weight for the edge (8,10) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,8, g.hasEdge(8, 10));
			assertEquals(".hasEdge() returned a wrong weight for the edge (10,9) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,11,g.hasEdge(10, 9));
			assertEquals(".hasEdge() returned a wrong weight for the edge (7,11) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,9, g.hasEdge(7, 11));
			assertEquals(".hasEdge() returned a wrong weight for the edge (16,12) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,15,g.hasEdge(16,12));
			
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	@Test
	public void testHasEdgeForUndirectedGraphWithOpposites() {
		try {
			g = this.initializeLargeGraph();

			String strEdges = "(v1,v2,weight) [(0,2,1),(2,5,2),(2,6,3),(6,7,4),(4,3,5),(7,3,6),(8,3,7),(8,10,8),(7,11,9),(1,9,10),(10,9,11),(13,14,12),(14,15,13),(15,16,14),(16,12,15),(16,17,16)]";
			
			assertEquals(".hasEdge() returned a wrong weight for the edge (2,0) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,1, g.hasEdge(2, 0));
						
			assertEquals(".hasEdge() returned a wrong weight for the edge (5,2) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,2, g.hasEdge(5, 2));
			assertEquals(".hasEdge() returned a wrong weight for the edge (6,2) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,3, g.hasEdge(6, 2));
			assertEquals(".hasEdge() returned a wrong weight for the edge (7,6) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,4, g.hasEdge(7, 6));
			assertEquals(".hasEdge() returned a wrong weight for the edge (3,4) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,5, g.hasEdge(3, 4));
			assertEquals(".hasEdge() returned a wrong weight for the edge (3,8) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,7, g.hasEdge(3, 8));
			assertEquals(".hasEdge() returned a wrong weight for the edge (10,8) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,8, g.hasEdge(10, 8));
			assertEquals(".hasEdge() returned a wrong weight for the edge (9,10) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,11,g.hasEdge(9, 10));
			assertEquals(".hasEdge() returned a wrong weight for the edge (11,7) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,9, g.hasEdge(11, 7));
			assertEquals(".hasEdge() returned a wrong weight for the edge (12,16) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,15,g.hasEdge(12,16));
			
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	@Test
	public void testHasEdgeForUndirectedGraphWithNotExistingEdges() {
		try {
			g = this.initializeLargeGraph();

			String strEdges = "(v1,v2,weight) [(0,2,1),(2,5,2),(2,6,3),(6,7,4),(4,3,5),(7,3,6),(8,3,7),(8,10,8),(7,11,9),(1,9,10),(10,9,11),(13,14,12),(14,15,13),(15,16,14),(16,12,15),(16,17,16)]";
			
			assertEquals(".hasEdge() returned a wrong weight for the not existing edge (2,10) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,-1, g.hasEdge(2, 10));
						
			assertEquals(".hasEdge() returned a wrong weight for the not existing edge (5,0) \n\t of the graph "+printVerticesArray(g)+
					"\n\t with the edges "+strEdges,-1, g.hasEdge(5, 0));
			
			
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	int [][] expectedMatrix_directedGraph = {
			{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0},	
			{0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
	};
	
	int [][] expectedMatrix_undirectedGraph = {
			{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0},	
			{1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,1,0,0,1,1,0,0,0,0,0,0,0,0,0},	
			{0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	
			{0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,0},	
			{0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0},	
			{0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0},	
			{0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,1},	
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0}
	};
	
	@Test
	public void testGetAdjacencyMatrix() {
		try {
	
			g = this.initializeLargeGraph();
			String strEdges = "(v1,v2,weight) [(0,2,1),(2,5,2),(2,6,3),(6,7,4),(4,3,5),(7,3,6),(8,3,7),(8,10,8),(7,11,9),(1,9,10),(10,9,11),(13,14,12),(14,15,13),(15,16,14),(16,12,15),(16,17,16)]";
			
			int[][] expectedMatrix = null;
			
			if(undirectedGraphFlag) {
				expectedMatrix = expectedMatrix_undirectedGraph; 
			} else {
				expectedMatrix = expectedMatrix_directedGraph;
			}
			
			int [][] matrix = g.getAdjacencyMatrix();
			
			StringBuilder sb = new StringBuilder();
			
	
			for (int i=0; i < matrix.length; i++) {
				for(int j=0; j < matrix.length; j++) {
					if(matrix[i][j] != expectedMatrix[i][j]) {
						sb.append("\t-> Error @ matrix position: ["+i+","+j+"]\n");
					}
				}
			}
			
			assertTrue(".getAdjacencyMatrix() failed \n\tfor the graph "+printVerticesArray(g) +
					"\n\twith the edges "+ strEdges +
					"\n\tat the following positions:\n"+sb.toString(),sb.length()==0);
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	

	@Test
	public void testNumAdjacentVerticesOfUndirectedGraph() {
		try {
			g = this.initializeLargeGraph();
			String strEdges = "(v1,v2,weight) [(0,2,1),(2,5,2),(2,6,3),(6,7,4),(4,3,5),(7,3,6),(8,3,7),(8,10,8),(7,11,9),(1,9,10),(10,9,11),(13,14,12),(14,15,13),(15,16,14),(16,12,15),(16,17,16)]";
			
			MyVertex[] v0 = g.getAdjacentVertices(0); 	
			assertEquals(".getAdjacentVertices() failed for vertex with index 0 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v0.length);
			
			MyVertex[] v1 = g.getAdjacentVertices(1);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 1 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v1.length);
			MyVertex[] v2 = g.getAdjacentVertices(2);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 2 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",3, v2.length);
			MyVertex[] v3 = g.getAdjacentVertices(3);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 3 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",3, v3.length);
			MyVertex[] v4 = g.getAdjacentVertices(4);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 4 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v4.length);
			MyVertex[] v5 = g.getAdjacentVertices(5);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 5 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v5.length);
			MyVertex[] v6 = g.getAdjacentVertices(6);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 6 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",2, v6.length);
			MyVertex[] v7 = g.getAdjacentVertices(7);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 7 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",3, v7.length);
			MyVertex[] v8 = g.getAdjacentVertices(8);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 8 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",2, v8.length);
	
			MyVertex[] v9 = g.getAdjacentVertices(9); 	
			assertEquals(".getAdjacentVertices() failed for vertex with index 9 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",2, v9.length);
			MyVertex[] v10 = g.getAdjacentVertices(10);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 10 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",2, v10.length);
			MyVertex[] v11 = g.getAdjacentVertices(11);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 11 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v11.length);
			MyVertex[] v12 = g.getAdjacentVertices(12);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 12 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v12.length);
			MyVertex[] v13 = g.getAdjacentVertices(13);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 13 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v13.length);
			MyVertex[] v14 = g.getAdjacentVertices(14);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 14 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",2, v14.length);
			MyVertex[] v15 = g.getAdjacentVertices(15);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 15 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",2, v15.length);
			MyVertex[] v16 = g.getAdjacentVertices(16);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 16 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",3, v16.length);
			MyVertex[] v17 = g.getAdjacentVertices(17);	
			assertEquals(".getAdjacentVertices() failed for vertex with index 17 "+
					"\n\tof the graph "+printVerticesArray(g)+
					"\n\twith the edges "+strEdges+"\n\n\t",1, v17.length);
		}catch (Exception ex) {
			assertTrue("Could not test as an exception has been thrown: "+ex,ex == null);
		}
	}
	
	
		
	//--------------------------------------
	//--- PRIVATE Methods
	private Graph initializeGraph2Components() {
		Graph map = new Graph();
		
		int linz = map.insertVertex(new MyVertex("Linz"));
		int stpoelten = map.insertVertex(new MyVertex("St.Poelten"));
		int wien = map.insertVertex(new MyVertex("Wien"));
		int innsbruck = map.insertVertex(new MyVertex("Innsbruck"));
		int bregenz = map.insertVertex(new MyVertex("Bregenz"));
		int eisenstadt = map.insertVertex(new MyVertex("Eisenstadt"));
		int graz = map.insertVertex(new MyVertex("Graz"));
		int klagenfurt = map.insertVertex(new MyVertex("Klagenfurt"));
		int salzburg = map.insertVertex(new MyVertex("Salzburg"));
		
		map.insertEdge(linz, wien, 1);
		map.insertEdge(wien, eisenstadt, 2);
		map.insertEdge(wien, graz, 3);
		map.insertEdge(graz, klagenfurt, 4);
		map.insertEdge(bregenz, innsbruck, 5);
		map.insertEdge(klagenfurt, innsbruck, 6);
		map.insertEdge(salzburg, innsbruck, 7);
		
		int london = map.insertVertex(new MyVertex("London"));

		map.insertEdge(stpoelten, london, 10);

		return map;
	}
	
	private Graph initializeLargeGraph() {
		Graph map = new Graph();
		
		int linz = map.insertVertex(new MyVertex("Linz"));				// 0
		int stpoelten = map.insertVertex(new MyVertex("St.Poelten"));	// 1
		int wien = map.insertVertex(new MyVertex("Wien"));				// 2
		int innsbruck = map.insertVertex(new MyVertex("Innsbruck"));	// 3
		int bregenz = map.insertVertex(new MyVertex("Bregenz"));		// 4
		int eisenstadt = map.insertVertex(new MyVertex("Eisenstadt"));	// 5
		int graz = map.insertVertex(new MyVertex("Graz"));				// 6
		int klagenfurt = map.insertVertex(new MyVertex("Klagenfurt"));	// 7
		int salzburg = map.insertVertex(new MyVertex("Salzburg"));		// 8
		
		map.insertEdge(linz, wien, 1);				// (0,2,1)
		map.insertEdge(wien, eisenstadt, 2);		// (2,5,2)
		map.insertEdge(wien, graz, 3);				// (2,6,3)
		map.insertEdge(graz, klagenfurt, 4);		// (6,7,4)
		map.insertEdge(bregenz, innsbruck, 5);		// (4,3,5)
		map.insertEdge(klagenfurt, innsbruck, 6);	// (7,3,6)
		map.insertEdge(salzburg, innsbruck, 7);		// (8,3,7)
		
		int a = map.insertVertex(new MyVertex("A"));	// 9
		int b = map.insertVertex(new MyVertex("B"));	// 10
		int c = map.insertVertex(new MyVertex("C"));	// 11
		int d = map.insertVertex(new MyVertex("D"));	// 12
		int e = map.insertVertex(new MyVertex("E"));	// 13
		int f = map.insertVertex(new MyVertex("F"));	// 14
		int g = map.insertVertex(new MyVertex("G"));	// 15
		int h = map.insertVertex(new MyVertex("H"));	// 16
		int i = map.insertVertex(new MyVertex("I"));	// 17
		
		map.insertEdge(salzburg, b, 8);		// (8,10,8)
		map.insertEdge(klagenfurt, c, 9);	// (7,11,9)
		map.insertEdge(stpoelten, a, 10);	// (1,9,10)
		map.insertEdge(b, a, 11);			// (10,9,11)

		map.insertEdge(e, f, 12);			// (13,14,12)
		map.insertEdge(f, g, 13);			// (14,15,13)
		map.insertEdge(g, h, 14);			// (15,16,14)
		map.insertEdge(h, d, 15);			// (16,12,15)
		map.insertEdge(h, i, 16);			// (16,17,16)
		
		try{
			map.insertEdge(i, i, 1);
		} catch (Exception ex) {
			assertTrue(ex instanceof IllegalArgumentException);
		}
		
		return map;
	}
	
		
	private String printVerticesArray(Graph g) {
		StringBuilder sb = new StringBuilder();
		MyVertex[] v = g.getVertices();
		
		sb.append("[");
		for (int i = 0; i < v.length; i++) {
			sb.append(i+"=\""+v[i].toString()+"\",");
		}
		sb.replace(sb.length()-1, sb.length(), "]");
		
		return sb.toString();
	}
}
